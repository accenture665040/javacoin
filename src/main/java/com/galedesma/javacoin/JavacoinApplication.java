package com.galedesma.javacoin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavacoinApplication {

  public static void main(String[] args) {
    SpringApplication.run(JavacoinApplication.class, args);
  }

}
