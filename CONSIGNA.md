# Torneo Cápsula Message Brokers - Javacoin

El banco Universal decide incorporar una nueva criptomoneda llamada Javacoin y permite a todos sus
usuarios comerciarla entre ellos. Para ello dispone de una billetera digital propia donde cada
cliente dispondrá de su saldo correspondiente.

El ejercicio consiste en desarrollar la transacción de compra/venta de Javacoins de punta a punta.
La comunicación será asincrónica vía mensajes JMS (Active Mq o Rabbit Mq) entre 3 partes:

**Plataforma Web:** puede ser un main o test unitario, no se pide interfaz gráfica. Desde aquí el
comprador y el vendedor envían sus mensajes de compra/venta.

**Banco:** contiene la info de las cuentas en dólares de cada usuario y sus DNI.

**Billetera:** contiene el saldo en Javacoin de cada usuario y sus DNI.

**Lógica de la transacción:**

1. Comprador envía msj de compra indicando cantidad de Javacoin a comprar y cotización propuesta.

2. Banco recibe y genera ID de órden. Valida usuario y saldo suficiente en dólares en la cuenta (
   órden + comisión). Descuenta saldo y envía msj a Billetera.

3. Billetera valida usuarios (comprador y vendedor), saldo suficiente de Javacoin en vendedor y
   registra la órden.

4. Vendedor acepta órden y se envía msj de órden aceptada a Billetera.

5. Billetera descuenta saldo de Javacoin en vendedor y acredita en comprador. Envía msj a Banco.

6. Banco valida saldo en dólares en vendedor para comisión de venta. Si está OK descuenta comisión y
   envía msj de operación exitosa informando nuevo saldo en Javacoin.

En caso de que el vendedor rechace la órden o cualquier validación falle, la órden se rechaza y
rollbackea enviando msj de error al comprador.

**Cálculo de comisiones:** 5% menos de 3 operaciones, 3% entre 3 y 6 operaciones, más de 6 operaciones
bonificadas.

**Nota:** Todos los datos de la app serán manejados en memoria, no se pide ningún modelo de
persistencia. El ejercicio estará 100% enfocado en el correcto manejo de la mensajería.